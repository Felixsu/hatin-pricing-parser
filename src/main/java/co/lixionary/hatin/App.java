package co.lixionary.hatin;

import java.util.List;
import java.util.Map;

public class App {

    public static void main(String[] args) {

        if (args.length != 4) {
            throw new RuntimeException("please pass correct arguments: filePath, DBUser, DBPass");
        }

        final Config config = new Config(args);
        config.print();

        final Reader reader = new Reader(config.getFilePath());
        final List<String> trackingIds = reader.read();

        final JdbcService jdbcService = new JdbcService(config.getDbUser(), config.getDbPass(), config.getDbUri());

        final Map<String, Double> map = jdbcService.execute(trackingIds);

        double total = map.entrySet().stream()
            .peek(e -> System.out.println(e.getKey() + "," + e.getValue()))
            .map(Map.Entry::getValue)
            .reduce(0.0, (curr, accu) -> {
                accu += curr;
                return accu;
            });

        System.out.println(total);
    }

}
