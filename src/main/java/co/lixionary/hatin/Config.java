package co.lixionary.hatin;

public class Config {

    private final String dbUser;
    private final String dbPass;
    private final String dbUri;
    private final String filePath;

    public Config(String dbUser, String dbPass, String filePath, String dbUri) {
        this.filePath = filePath;
        this.dbUser = dbUser;
        this.dbPass = dbPass;
        this.dbUri = dbUri;
    }

    public Config(String[] args) {
        this.filePath = args[0];
        this.dbUser = args[1];
        this.dbPass = args[2];
        this.dbUri = args[3];
    }

    public String getDbUser() {
        return dbUser;
    }

    public String getDbPass() {
        return dbPass;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getDbUri() {
        return dbUri;
    }

    public void print() {
        System.out.println("File Path " + filePath);
        System.out.println("DB User " + dbUser);
        System.out.println("DB Pass " + dbPass);
        System.out.println("DB Uri " + dbUri);
    }

}
