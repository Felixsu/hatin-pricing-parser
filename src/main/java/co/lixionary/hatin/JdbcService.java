package co.lixionary.hatin;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JdbcService {

    private final String dbUser;
    private final String dbPass;
    private final String dbUri;

    public JdbcService(String dbUser, String dbPass, String dbUri) {
        this.dbUser = dbUser;
        this.dbPass = dbPass;
        this.dbUri = dbUri;
    }

    public Map<String, Double> execute(List<String> trackingIds) {
        final Map<String, Double> result = new HashMap<>();
        final ObjectMapper mapper = new ObjectMapper();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try (final Connection conn = DriverManager.getConnection(dbUri, dbUser, dbPass)){

            final String trackingIdsStr = trackingIds.stream()
                .map(tid -> "\"" + tid + "\"")
                .collect(Collectors.joining(","));
            final String sql = "SELECT o.tracking_id, o.pricing_info " +
                "FROM orders o " +
                "WHERE o.tracking_id IN (" + trackingIdsStr + ")";

            System.out.println(sql);

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                final String trackingId = resultSet.getString("tracking_id");
                final String pricingInfo = resultSet.getString("pricing_info");

                JsonNode node = mapper.readTree(pricingInfo);
                result.put(trackingId, node.get("delivery_fee").doubleValue());
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }



}
