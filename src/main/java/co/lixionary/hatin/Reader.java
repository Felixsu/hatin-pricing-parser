package co.lixionary.hatin;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reader {

    private final String file;

    public Reader(String file) {
        this.file = file;
    }

    public List<String> read() {
        final List<String> trackingIds = new ArrayList<>();
        try(CSVReader reader = new CSVReader(new FileReader(file))) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                trackingIds.add(line[0]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return trackingIds;
    }

}
